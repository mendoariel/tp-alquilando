import { UsersService } from './../../services/users.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-usuario',
  templateUrl: './new-usuario.component.html',
  styleUrls: ['./new-usuario.component.css']
})
export class NewUsuarioComponent implements OnInit {

  userForm: FormGroup;
  spinner = true;
  editUser: any;

  constructor(
                @Inject(MAT_DIALOG_DATA) public passedUser: any,
                private formBuilder: FormBuilder,
                private userService: UsersService
              ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      first_name: [''],
      last_name: [''],
      email: [''],
    });
    if(this.passedUser.title === 'Editar') {
      this.putUserOnFormToEdit();
    }
    if(this.passedUser.title === 'Eliminar') {
      this.putUserOnFormToDelete();
    }
    
    this.spinner = false;
  }
  putUserOnFormToEdit() {
    this.first_name.setValue(this.passedUser.user.first_name);
    this.last_name.setValue(this.passedUser.user.last_name);
    this.email.setValue(this.passedUser.user.email);
  }
  putUserOnFormToDelete() {
    this.first_name.setValue(this.passedUser.user.first_name);
    this.last_name.setValue(this.passedUser.user.last_name);
    this.email.setValue(this.passedUser.user.email);

    this.first_name.disable();
    this.last_name.disable();
    this.email.disable();
  }

  onSubmit() {
    this.spinner = true;
    if (this.passedUser.title === "Editar") {
      this.userService.editUser(this.userForm.value, this.passedUser.user.id).subscribe(
        result => {
          // console.log(result);
          this.spinner = false;
          console.log(this.editUser);
        },
        error => {
          console.log(error);
          this.spinner = false;
          
         });
    }
    if (this.passedUser.title === "Nuevo") {
      this.userService.newUser(this.userForm.value).subscribe(
        result => {
          console.log(result);
          this.spinner = false;
        },
        error => {
          console.log(error);
          this.spinner = false;
          
         });
    }
    
  }
  get first_name() {
    return this.userForm.get('first_name');
  }
  get last_name() {
    return this.userForm.get('last_name');
  }
  get email() {
    return this.userForm.get('email');
  }
}
