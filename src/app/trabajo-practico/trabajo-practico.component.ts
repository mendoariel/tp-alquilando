import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, PageEvent } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { NewUsuarioComponent } from './new-usuario/new-usuario.component';
import { UsersService } from '../services/users.service';
import { DetailComponent } from './detail/detail.component';

@Component({
  selector: 'app-trabajo-practico',
  templateUrl: './trabajo-practico.component.html',
  styleUrls: ['./trabajo-practico.component.css']
})
export class TrabajoPracticoComponent implements OnInit {
  users: any[] = [];
  displayedColumns: string[] = ['first_name', 'last_name', 'email', 'detail','edit', 'delete'];
  spinner = true;
  dataSource;
  length;
  pageSize;
  pageIndex;
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;


  constructor(
      private dialog: MatDialog, 
      private dialogDetailUser: MatDialog, 
      private userService: UsersService) { }

  ngOnInit() {
  
    this.dataSource = new MatTableDataSource();   
    this.dataSource.paginator = this.paginator;
    this.pageEvent = new PageEvent();
    this.pageEvent.pageIndex = 0;
    this.pageEvent.pageSize = 6;

    this.getServerData(this.pageEvent);
  }
  
  public getServerData(event?:PageEvent){
    this.userService.getAllUser(event.pageIndex + 1, event.pageSize).subscribe(
      response =>{
        if(response.error) {
          this.spinner = false;
          // handle error
          console.log(response.error);
        } else {
          this.spinner = false;
          this.dataSource = response.data;
          this.pageIndex = response.pageIndex;
          this.pageSize = response.per_page;
          this.length = response.total;
        }
      },
      error =>{
        // handle error
        this.spinner = false;
        console.log(error);
      }
    );
    return event;
  }

  
  newUsuario() {
    const dialogRef = this.dialog.open(NewUsuarioComponent, {
      data: {
        user: null,
        title: 'Nuevo'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getServerData(this.pageEvent);
      console.log(result);
    });
  }

  editUser(user) {
    const dialogRef = this.dialog.open(NewUsuarioComponent, {
      data: {
        user: user,
        title: 'Editar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getServerData(this.pageEvent);
      console.log(result);
    });;
  }

  detailUser(user) {
    const dialogRef = this.dialog.open(DetailComponent, {
      data: {
        id: user.id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getServerData(this.pageEvent);
      console.log(result);
    });;
  }
  
  deleteUser(user) { 
    const dialogRef = this.dialog.open(NewUsuarioComponent, {
      data: {
        user: user,
        title: 'Eliminar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getServerData(this.pageEvent);
      console.log(result);
    });
  }

}
