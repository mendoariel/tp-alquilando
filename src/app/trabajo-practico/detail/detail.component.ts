import { UsersService } from './../../services/users.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  spinner = true;
  user: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public passedUser: any,
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.userService.getDetailUser(this.passedUser.id).subscribe(
      result => {
        this.spinner = false;
        this.user = result;
        console.log(this.user);
      }, 
      error => {
        this.spinner = false;
        console.log(error);
      });
  }

}
