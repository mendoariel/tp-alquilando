import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrabajoPracticoComponent } from './trabajo-practico.component';

describe('TrabajoPracticoComponent', () => {
  let component: TrabajoPracticoComponent;
  let fixture: ComponentFixture<TrabajoPracticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrabajoPracticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrabajoPracticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
