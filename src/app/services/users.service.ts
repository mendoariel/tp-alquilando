import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  newUser(body) {
    return this.http.post<any>(`https://reqres.in/api/users/`, body);
  }
  getAllUser(page, pageSize) {
    return this.http.get<any>(`https://reqres.in/api/users?page=${page}&per_page=${pageSize}`);
  }
  getDetailUser(id) {
    return this.http.get<any>(`https://reqres.in/api/users/${id}`);
  }
  editUser(body, id) {
    return this.http.put<any>(`https://reqres.in/api/users/${id}`, body);
  }
}
