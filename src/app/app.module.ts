import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { TrabajoPracticoComponent } from './trabajo-practico/trabajo-practico.component';
import { UsersService } from './services/users.service';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { NewUsuarioComponent } from './trabajo-practico/new-usuario/new-usuario.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailComponent } from './trabajo-practico/detail/detail.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    TrabajoPracticoComponent,
    HeaderComponent,
    SidenavListComponent,
    NewUsuarioComponent,
    DetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [
    UsersService
  ],
  entryComponents: [
    NewUsuarioComponent,
    DetailComponent
      ],
  bootstrap: [AppComponent]
})
export class AppModule { }
